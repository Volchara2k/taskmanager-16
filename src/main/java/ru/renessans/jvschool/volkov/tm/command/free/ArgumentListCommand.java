package ru.renessans.jvschool.volkov.tm.command.free;

import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;
import ru.renessans.jvschool.volkov.tm.exception.security.NotifyAddFailureException;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

import java.util.Collection;

public final class ArgumentListCommand extends AbstractCommand {

    private static final String CMD_ARGUMENTS = "arguments";

    private static final String ARG_ARGUMENTS = "-arg";

    private static final String DESC_ARGUMENTS = "вывод списка поддерживаемых программных аргументов";

    private static final String NOTIFY_ARGUMENTS = "Список поддерживаемых программных аргументов: \n";

    @Override
    public String getCommand() {
        return CMD_ARGUMENTS;
    }

    @Override
    public String getArgument() {
        return ARG_ARGUMENTS;
    }

    @Override
    public String getDescription() {
        return DESC_ARGUMENTS;
    }

    @Override
    public void execute() {
        ViewUtil.getInstance().print(NOTIFY_ARGUMENTS);
        final Collection<AbstractCommand> arguments = super.serviceLocator.getCommandService().allArgumentCommands();
        if (ValidRuleUtil.isNullOrEmpty(arguments))
            throw new NotifyAddFailureException("Программные аргументы");
        ViewUtil.getInstance().print(arguments);
    }

}