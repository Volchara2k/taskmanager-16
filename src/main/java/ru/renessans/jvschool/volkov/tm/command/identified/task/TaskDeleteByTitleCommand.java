package ru.renessans.jvschool.volkov.tm.command.identified.task;

import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;
import ru.renessans.jvschool.volkov.tm.model.Task;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class TaskDeleteByTitleCommand extends AbstractCommand {

    private static final String CMD_TASK_DELETE_BY_TITLE = "task-delete-by-title";

    private static final String DESC_TASK_DELETE_BY_TITLE = "удалить задачу по заголовку";

    private static final String NOTIFY_TASK_DELETE_BY_TITLE =
            "Для удаления задачи по заголовку введите имя заголовок из списка ниже.\n";

    @Override
    public String getCommand() {
        return CMD_TASK_DELETE_BY_TITLE;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESC_TASK_DELETE_BY_TITLE;
    }

    @Override
    public void execute() {
        final String userId = super.serviceLocator.getAuthService().getUserId();
        ViewUtil.getInstance().print(NOTIFY_TASK_DELETE_BY_TITLE);
        final String title = ViewUtil.getInstance().getLine();
        final Task task = super.serviceLocator.getTaskService().removeByTitle(userId, title);
        ViewUtil.getInstance().print(task);
    }

}