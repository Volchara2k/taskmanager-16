package ru.renessans.jvschool.volkov.tm.command.identified.task;

import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;
import ru.renessans.jvschool.volkov.tm.model.Task;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

import java.util.Collection;

public final class TaskListCommand extends AbstractCommand {

    private static final String CMD_TASK_LIST = "task-list";

    private static final String DESC_TASK_LIST = "вывод списка задач";

    private static final String NOTIFY_TASK_LIST = "Текущий список задач: \n";

    @Override
    public String getCommand() {
        return CMD_TASK_LIST;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESC_TASK_LIST;
    }

    @Override
    public void execute() {
        final String userId = super.serviceLocator.getAuthService().getUserId();
        ViewUtil.getInstance().print(NOTIFY_TASK_LIST);
        final Collection<Task> tasks = super.serviceLocator.getTaskService().getAll(userId);
        ViewUtil.getInstance().print(tasks);
    }

}