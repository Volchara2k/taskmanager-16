package ru.renessans.jvschool.volkov.tm.exception.incorrect;

import ru.renessans.jvschool.volkov.tm.exception.AbstractRuntimeException;

public final class IncorrectIndexException extends AbstractRuntimeException {

    private static final String INDEX_NOT_INTEGER =
            "Ошибка! Значение \"%s\" параметра \"индекс\" не является целочисленным типом данных!\n";

    private static final String INDEX_INCORRECT = "Ошибка! Параметр \"индекс\" является некорректным!\n";

    public IncorrectIndexException() {
        super(INDEX_INCORRECT);
    }

    public IncorrectIndexException(final String message) {
        super(String.format(INDEX_NOT_INTEGER, message));
    }

}