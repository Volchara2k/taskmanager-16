package ru.renessans.jvschool.volkov.tm.model;

import ru.renessans.jvschool.volkov.tm.enumeration.UserRole;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

import java.util.Objects;

public class User extends AbstractModel {

    private String login = "";

    private String passwordHash = "";

    private String firstName = "";

    private String lastName = "";

    private String middleName = "";

    private UserRole role = UserRole.USER;

    private Boolean lockdown = false;

    public User() {
    }

    public User(final String login, final String passwordHash) {
        this.login = login;
        this.passwordHash = passwordHash;
    }

    public User(final String login, final String password, final UserRole role) {
        this.login = login;
        this.passwordHash = password;
        this.role = role;
    }

    public User(final String login, final String password, final String firstName) {
        this.login = login;
        this.passwordHash = password;
        this.firstName = firstName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(final String login) {
        this.login = login;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(final String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(final String middleName) {
        this.middleName = middleName;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(final UserRole role) {
        this.role = role;
    }

    public Boolean getLockdown() {
        return lockdown;
    }

    public void setLockdown(final Boolean lockdown) {
        this.lockdown = lockdown;
    }

    @Override
    public String toString() {
        final StringBuilder result = new StringBuilder();
        if (!ValidRuleUtil.isNullOrEmpty(this.login))
            result.append("Логин: ").append(login);
        if (!ValidRuleUtil.isNullOrEmpty(this.firstName))
            result.append(", имя: ").append(this.firstName).append("\n");
        if (!ValidRuleUtil.isNullOrEmpty(this.lastName))
            result.append(", фамилия: ").append(this.lastName).append("\n");
        if (!Objects.isNull(this.role)) {
            result.append("\nРоль: ").append(this.role.getTitle()).append("\n");
        }
        return result.toString();
    }


}