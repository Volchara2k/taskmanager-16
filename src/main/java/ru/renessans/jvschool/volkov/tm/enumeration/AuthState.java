package ru.renessans.jvschool.volkov.tm.enumeration;

public enum AuthState {

    SUCCESS("Успешно!"),

    APPLICATION_ERROR("Ошибка приложения!"),

    USER_NOT_FOUND("Пользователь не найден!"),

    INVALID_PASSWORD("Некорректный пароль!"),

    NO_ACCESS_RIGHTS("Нет прав доступа!"),

    LOCKDOWN_PROFILE("Профиль заблокирован!");

    private final String title;

    AuthState(final String title) {
        this.title = title;
    }

    public String getTitle() {
        return this.title;
    }

    public boolean isSuccess() {
        return this == SUCCESS;
    }

}