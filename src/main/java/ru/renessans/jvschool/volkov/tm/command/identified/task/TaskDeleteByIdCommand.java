package ru.renessans.jvschool.volkov.tm.command.identified.task;

import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;
import ru.renessans.jvschool.volkov.tm.model.Task;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class TaskDeleteByIdCommand extends AbstractCommand {

    private static final String CMD_TASK_DELETE_BY_ID = "task-delete-by-id";

    private static final String DESC_TASK_DELETE_BY_ID = "удалить задачу по идентификатору";

    private static final String NOTIFY_TASK_DELETE_BY_ID =
            "Для удаления задачи по идентификатору введите идентификатор задачи из списка.\n";

    @Override
    public String getCommand() {
        return CMD_TASK_DELETE_BY_ID;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESC_TASK_DELETE_BY_ID;
    }

    @Override
    public void execute() {
        final String userId = super.serviceLocator.getAuthService().getUserId();
        ViewUtil.getInstance().print(NOTIFY_TASK_DELETE_BY_ID);
        final String id = ViewUtil.getInstance().getLine();
        final Task task = super.serviceLocator.getTaskService().removeById(userId, id);
        ViewUtil.getInstance().print(task);
    }

}