package ru.renessans.jvschool.volkov.tm.command.free;

import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;
import ru.renessans.jvschool.volkov.tm.exception.security.NotifyAddFailureException;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

import java.util.Collection;

public final class HelpListCommand extends AbstractCommand {

    private static final String CMD_HELP = "help";

    private static final String ARG_HELP = "-h";

    private static final String DESC_HELP = "вывод списка команд";

    private static final String NOTIFY_HELP = "Список команд";

    @Override
    public String getCommand() {
        return CMD_HELP;
    }

    @Override
    public String getArgument() {
        return ARG_HELP;
    }

    @Override
    public String getDescription() {
        return DESC_HELP;
    }

    @Override
    public void execute() {
        ViewUtil.getInstance().print(NOTIFY_HELP);
        final Collection<AbstractCommand> commands = super.serviceLocator.getCommandService().allCommands();
        if (ValidRuleUtil.isNullOrEmpty(commands))
            throw new NotifyAddFailureException("Описание команд");
        ViewUtil.getInstance().print(commands);
    }

}