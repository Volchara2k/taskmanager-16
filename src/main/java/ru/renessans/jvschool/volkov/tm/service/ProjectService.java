package ru.renessans.jvschool.volkov.tm.service;

import ru.renessans.jvschool.volkov.tm.api.repository.ICrudRepository;
import ru.renessans.jvschool.volkov.tm.api.service.ICrudService;
import ru.renessans.jvschool.volkov.tm.exception.empty.*;
import ru.renessans.jvschool.volkov.tm.exception.incorrect.IncorrectIndexException;
import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.model.User;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

public final class ProjectService implements ICrudService<Project> {

    private final ICrudRepository<Project> projectRepository;

    public ProjectService(final ICrudRepository<Project> projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public Project add(final String userId, final String title, final String description) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new EmptyTitleException();
        if (ValidRuleUtil.isNullOrEmpty(description)) throw new EmptyDescriptionException();

        final Project project = new Project(title, description);
        return this.projectRepository.add(userId, project);
    }

    @Override
    public Project updateByIndex(final String userId, final Integer index, final String title, final String description) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(index)) throw new IncorrectIndexException();
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new EmptyTitleException();
        if (ValidRuleUtil.isNullOrEmpty(description)) throw new EmptyDescriptionException();

        final Project project = getByIndex(userId, index);
        if (Objects.isNull(project)) throw new EmptyProjectException();
        project.setTitle(title);
        project.setDescription(description);

        return project;
    }

    @Override
    public Project updateById(final String userId, final String id, final String title, final String description) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new EmptyIdException();
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new EmptyTitleException();
        if (ValidRuleUtil.isNullOrEmpty(description)) throw new EmptyDescriptionException();

        final Project project = getById(userId, id);
        if (Objects.isNull(project)) throw new EmptyProjectException();
        project.setTitle(title);
        project.setDescription(description);

        return project;
    }

    @Override
    public Project removeByIndex(final String userId, final Integer index) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(index)) throw new IncorrectIndexException();
        return this.projectRepository.removeByIndex(userId, index);
    }

    @Override
    public Project removeById(final String userId, final String id) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (Objects.isNull(id)) throw new EmptyIdException();
        return this.projectRepository.removeById(userId, id);
    }

    @Override
    public Project removeByTitle(final String userId, final String title) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (Objects.isNull(title)) throw new EmptyTitleException();
        return this.projectRepository.removeByTitle(userId, title);
    }

    @Override
    public Collection<Project> removeAll(final String userId) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        return this.projectRepository.removeAll(userId);
    }

    @Override
    public Project getByIndex(final String userId, final Integer index) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(index)) throw new IncorrectIndexException();
        return this.projectRepository.getByIndex(userId, index);
    }

    @Override
    public Project getById(final String userId, final String id) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (Objects.isNull(id)) throw new EmptyIdException();
        return this.projectRepository.getById(userId, id);
    }

    @Override
    public Project getByTitle(final String userId, final String title) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (Objects.isNull(title)) throw new EmptyTitleException();
        return this.projectRepository.getByTitle(userId, title);
    }

    @Override
    public Collection<Project> getAll(final String userId) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        return this.projectRepository.getAll(userId);
    }

    @Override
    public Collection<Project> assignTestData(final Collection<User> users) {
        if (Objects.isNull(users)) throw new EmptyUserException();

        final List<Project> assignedData = new ArrayList<>();
        users.forEach(user -> {
            final Project project =
                    add(user.getId(), "title", "description");
            assignedData.add(project);
        });

        return assignedData;
    }

}