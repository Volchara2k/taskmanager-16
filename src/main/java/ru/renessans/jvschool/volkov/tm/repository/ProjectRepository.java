package ru.renessans.jvschool.volkov.tm.repository;

import ru.renessans.jvschool.volkov.tm.api.repository.ICrudRepository;
import ru.renessans.jvschool.volkov.tm.exception.empty.EmptyProjectException;
import ru.renessans.jvschool.volkov.tm.model.Project;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

public final class ProjectRepository implements ICrudRepository<Project> {

    private final List<Project> projects = new CopyOnWriteArrayList<>();

    @Override
    public Project add(final String userId, final Project project) {
        project.setUserId(userId);
        this.projects.add(project);
        return project;
    }

    @Override
    public Project removeByIndex(final String userId, final Integer index) {
        final Project project = getByIndex(userId, index);
        if (Objects.isNull(project)) throw new EmptyProjectException();
        return remove(userId, project);
    }

    @Override
    public Project removeById(final String userId, final String id) {
        final Project project = getById(userId, id);
        if (Objects.isNull(project)) return null;
        return remove(userId, project);
    }

    @Override
    public Project removeByTitle(final String userId, final String title) {
        final Project project = getByTitle(userId, title);
        if (Objects.isNull(project)) throw new EmptyProjectException();
        return remove(userId, project);
    }

    @Override
    public Project remove(final String userId, final Project project) {
        this.projects.remove(project);
        return project;
    }

    @Override
    public Collection<Project> removeAll(final String userId) {
        final List<Project> userProjects = new ArrayList<>();

        for (final Project project : this.projects) {
            if (userId.equals(project.getUserId())) {
                userProjects.add(project);
                this.projects.remove(project);
            }
        }

        return userProjects;
    }

    @Override
    public Project getByIndex(final String userId, final Integer index) {
        final List<Project> userProjects = new ArrayList<>();

        for (final Project project : this.projects) {
            if (userId.equals(project.getUserId())) userProjects.add(project);
        }

        return userProjects.get(index);
    }

    @Override
    public Project getById(final String userId, final String id) {
        for (final Project project : this.projects) {
            if (userId.equals(project.getUserId()) &&
                    id.equals(project.getId())) return project;
        }
        return null;
    }

    @Override
    public Project getByTitle(final String userId, final String title) {
        for (final Project project : this.projects) {
            if (userId.equals(project.getUserId()) &&
                    title.equals(project.getTitle())) return project;
        }
        return null;
    }

    @Override
    public Collection<Project> getAll(final String userId) {
        final List<Project> userProjects = new ArrayList<>();

        for (final Project project : this.projects) {
            if (userId.equals(project.getUserId())) userProjects.add(project);
        }

        return userProjects;
    }

}