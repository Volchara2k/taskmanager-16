package ru.renessans.jvschool.volkov.tm.command.admin;

import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;
import ru.renessans.jvschool.volkov.tm.enumeration.UserRole;
import ru.renessans.jvschool.volkov.tm.model.User;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class UserUnlockCommand extends AbstractCommand {

    private static final String CMD_USER_UNLOCK = "user-unlock";

    private static final String DESC_USER_UNLOCK = "разблокировать пользователя (администратор)";

    private static final String NOTIFY_USER_UNLOCK = "Для разблокирования пользователя в системе введите его логин. \n";

    @Override
    public String getCommand() {
        return CMD_USER_UNLOCK;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESC_USER_UNLOCK;
    }

    @Override
    public void execute() {
        ViewUtil.getInstance().print(NOTIFY_USER_UNLOCK);
        final String login = ViewUtil.getInstance().getLine();
        final User user = super.serviceLocator.getUserService().unlockUserByLogin(login);
        ViewUtil.getInstance().print(user);
    }

    @Override
    public UserRole[] executionRoles() {
        return new UserRole[]{UserRole.ADMIN};
    }

}