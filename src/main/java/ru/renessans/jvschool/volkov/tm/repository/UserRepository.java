package ru.renessans.jvschool.volkov.tm.repository;

import ru.renessans.jvschool.volkov.tm.api.repository.IUserRepository;
import ru.renessans.jvschool.volkov.tm.exception.empty.EmptyUserException;
import ru.renessans.jvschool.volkov.tm.model.User;

import java.util.*;

public final class UserRepository implements IUserRepository {

    private final Map<String, User> userMap = new LinkedHashMap<>();

    @Override
    public User add(final User user) {
        this.userMap.put(user.getId(), user);
        return user;
    }

    @Override
    public User getById(final String id) {
        for (final User user : this.userMap.values()) {
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    @Override
    public User getByLogin(final String login) {
        for (final User user : this.userMap.values()) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public Collection<User> getAll() {
        return this.userMap.values();
    }

    @Override
    public User removeById(final String id) {
        final User user = getById(id);
        if (Objects.isNull(user)) throw new EmptyUserException();
        return removeByUser(user);
    }

    @Override
    public User removeByLogin(final String login) {
        final User user = getByLogin(login);
        if (Objects.isNull(user)) throw new EmptyUserException();
        return removeByUser(user);
    }

    @Override
    public User removeByUser(final User user) {
        this.userMap.values().remove(user);
        return user;
    }

}