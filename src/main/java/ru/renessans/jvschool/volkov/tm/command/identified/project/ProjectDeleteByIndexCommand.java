package ru.renessans.jvschool.volkov.tm.command.identified.project;

import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;
import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class ProjectDeleteByIndexCommand extends AbstractCommand {

    private static final String CMD_PROJECT_DELETE_BY_INDEX = "project-delete-by-index";

    private static final String DESC_PROJECT_DELETE_BY_INDEX = "удалить проект по индексу";

    private static final String NOTIFY_PROJECT_DELETE_BY_INDEX =
            "Для удаления проекта по индексу введите индекс проекта из списка.\n";

    @Override
    public String getCommand() {
        return CMD_PROJECT_DELETE_BY_INDEX;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESC_PROJECT_DELETE_BY_INDEX;
    }

    @Override
    public void execute() {
        final String userId = super.serviceLocator.getAuthService().getUserId();
        ViewUtil.getInstance().print(NOTIFY_PROJECT_DELETE_BY_INDEX);
        final Integer index = ViewUtil.getInstance().getInteger() - 1;
        final Project project = super.serviceLocator.getProjectService().removeByIndex(userId, index);
        ViewUtil.getInstance().print(project);
    }

}