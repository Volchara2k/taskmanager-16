package ru.renessans.jvschool.volkov.tm.command.profile;

import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;
import ru.renessans.jvschool.volkov.tm.model.User;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class PasswordUpdateCommand extends AbstractCommand {

    private static final String CMD_UPDATE_PASSWORD = "update-password";

    private static final String ARG_UPDATE_PASSWORD = "обновить пароль пользователя";

    private static final String NOTIFY_UPDATE_PASSWORD = "Для смены пароля введите новый пароль: \n";

    @Override
    public String getCommand() {
        return CMD_UPDATE_PASSWORD;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return ARG_UPDATE_PASSWORD;
    }

    @Override
    public void execute() {
        final String userId = super.serviceLocator.getAuthService().getUserId();
        ViewUtil.getInstance().print(NOTIFY_UPDATE_PASSWORD);
        final String password = ViewUtil.getInstance().getLine();
        final User user = super.serviceLocator.getUserService().updatePasswordById(userId, password);
        ViewUtil.getInstance().print(user);
    }

}