package ru.renessans.jvschool.volkov.tm.command.identified.project;

import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;
import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class ProjectDeleteByTitleCommand extends AbstractCommand {

    private static final String CMD_PROJECT_DELETE_BY_TITLE = "project-delete-by-title";

    private static final String DESC_PROJECT_DELETE_BY_TITLE = "удалить проект по заголовку";

    private static final String NOTIFY_PROJECT_DELETE_BY_TITLE =
            "Для удаления заголовка по имени введите заголовок проекта из списка.\n";

    @Override
    public String getCommand() {
        return CMD_PROJECT_DELETE_BY_TITLE;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESC_PROJECT_DELETE_BY_TITLE;
    }

    @Override
    public void execute() {
        final String userId = super.serviceLocator.getAuthService().getUserId();
        ViewUtil.getInstance().print(NOTIFY_PROJECT_DELETE_BY_TITLE);
        final String title = ViewUtil.getInstance().getLine();
        final Project project = super.serviceLocator.getProjectService().removeByTitle(userId, title);
        ViewUtil.getInstance().print(project);
    }

}