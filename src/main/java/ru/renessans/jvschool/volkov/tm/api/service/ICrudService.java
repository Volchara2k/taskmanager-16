package ru.renessans.jvschool.volkov.tm.api.service;

import ru.renessans.jvschool.volkov.tm.model.User;

import java.util.Collection;

public interface ICrudService<T> {

    T add(String userId, String title, String description);

    T updateByIndex(String userId, Integer index, String title, String description);

    T updateById(String userId, String id, String title, String description);

    T removeByIndex(String userId, Integer index);

    T removeById(String userId, String id);

    T removeByTitle(String userId, String title);

    Collection<T> removeAll(String userId);

    T getByIndex(String userId, Integer index);

    T getById(String userId, String id);

    T getByTitle(String userId, String title);

    Collection<T> getAll(String userId);

    Collection<T> assignTestData(Collection<User> users);

}