package ru.renessans.jvschool.volkov.tm.api.service;

import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.model.Task;

public interface IServiceLocator {

    IUserService getUserService();

    IAuthService getAuthService();

    ICrudService<Task> getTaskService();

    ICrudService<Project> getProjectService();

    ICommandService getCommandService();

}