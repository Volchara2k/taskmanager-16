package ru.renessans.jvschool.volkov.tm.command.identified.task;

import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;
import ru.renessans.jvschool.volkov.tm.model.Task;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class TaskCreateCommand extends AbstractCommand {

    private static final String CMD_TASK_CREATE = "task-create";

    private static final String DESC_TASK_CREATE = "добавить новую задачу";

    private static final String NOTIFY_TASK_CREATE =
            "Для создания задачи введите её заголовок или заголовок с описанием.";

    @Override
    public String getCommand() {
        return CMD_TASK_CREATE;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESC_TASK_CREATE;
    }

    @Override
    public void execute() {
        final String userId = super.serviceLocator.getAuthService().getUserId();
        ViewUtil.getInstance().print(NOTIFY_TASK_CREATE);
        final String title = ViewUtil.getInstance().getLine();
        final String description = ViewUtil.getInstance().getLine();
        final Task task = super.serviceLocator.getTaskService().add(userId, title, description);
        ViewUtil.getInstance().print(task);
    }

}