package ru.renessans.jvschool.volkov.tm.command.identified.task;

import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;
import ru.renessans.jvschool.volkov.tm.model.Task;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class TaskViewByIndexCommand extends AbstractCommand {

    private static final String CMD_TASK_VIEW_BY_INDEX = "task-view-by-index";

    private static final String DESC_TASK_VIEW_BY_INDEX = "просмотреть задачу по индексу";

    private static final String NOTIFY_TASK_VIEW_BY_INDEX = "Для отображения задачи по индексу введите индекс задачи из списка ниже.\n";

    @Override
    public String getCommand() {
        return CMD_TASK_VIEW_BY_INDEX;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESC_TASK_VIEW_BY_INDEX;
    }

    @Override
    public void execute() {
        final String userId = super.serviceLocator.getAuthService().getUserId();
        ViewUtil.getInstance().print(NOTIFY_TASK_VIEW_BY_INDEX);
        final Integer index = ViewUtil.getInstance().getInteger() - 1;
        final Task task = super.serviceLocator.getTaskService().getByIndex(userId, index);
        ViewUtil.getInstance().print(task);
    }

}