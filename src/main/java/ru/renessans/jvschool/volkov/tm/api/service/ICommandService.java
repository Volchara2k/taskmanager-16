package ru.renessans.jvschool.volkov.tm.api.service;

import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandService {

    Collection<AbstractCommand> initCommands();

    Collection<AbstractCommand> allCommands();

    Collection<AbstractCommand> allTerminalCommands();

    Collection<AbstractCommand> allArgumentCommands();

    AbstractCommand getTerminalCommand(final String command);

    AbstractCommand getArgumentCommand(final String command);

}