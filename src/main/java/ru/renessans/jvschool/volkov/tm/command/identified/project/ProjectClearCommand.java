package ru.renessans.jvschool.volkov.tm.command.identified.project;

import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;
import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

import java.util.Collection;

public final class ProjectClearCommand extends AbstractCommand {

    private static final String CMD_PROJECT_CLEAR = "project-clear";

    private static final String DESC_PROJECT_CLEAR = "очистить все проекты";

    private static final String NOTIFY_PROJECT_CLEAR = "Производится очистка списка проекты";

    @Override
    public String getCommand() {
        return CMD_PROJECT_CLEAR;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESC_PROJECT_CLEAR;
    }

    @Override
    public void execute() {
        final String userId = super.serviceLocator.getAuthService().getUserId();
        ViewUtil.getInstance().print(NOTIFY_PROJECT_CLEAR);
        final Collection<Project> projects = super.serviceLocator.getProjectService().removeAll(userId);
        ViewUtil.getInstance().print(projects);
    }

}