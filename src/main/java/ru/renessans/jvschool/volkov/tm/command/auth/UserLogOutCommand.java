package ru.renessans.jvschool.volkov.tm.command.auth;

import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class UserLogOutCommand extends AbstractCommand {

    private static final String CMD_LOG_OUT = "log-out";

    private static final String DESC_LOG_OUT = "выйти из системы";

    private static final String NOTIFY_LOG_OUT = "Производится выход пользователя из системы...";

    @Override
    public String getCommand() {
        return CMD_LOG_OUT;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESC_LOG_OUT;
    }

    @Override
    public void execute() {
        ViewUtil.getInstance().print(NOTIFY_LOG_OUT);
        final boolean logout = this.serviceLocator.getAuthService().logOut();
        ViewUtil.getInstance().print(logout);
    }

}