package ru.renessans.jvschool.volkov.tm.command.auth;

import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;
import ru.renessans.jvschool.volkov.tm.enumeration.AuthState;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class UserSignInCommand extends AbstractCommand {

    private static final String CMD_SIGN_IN = "sign-in";

    private static final String DESC_SIGN_IN = "войти в систему";

    private static final String NOTIFY_SIGN_IN = "Для авторизации пользователя в системе введите логин и пароль: \n";

    @Override
    public String getCommand() {
        return CMD_SIGN_IN;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESC_SIGN_IN;
    }

    @Override
    public void execute() {
        ViewUtil.getInstance().print(NOTIFY_SIGN_IN);
        final String login = ViewUtil.getInstance().getLine();
        final String password = ViewUtil.getInstance().getLine();
        final AuthState authState = super.serviceLocator.getAuthService().signIn(login, password);
        ViewUtil.getInstance().print(authState);
    }

}