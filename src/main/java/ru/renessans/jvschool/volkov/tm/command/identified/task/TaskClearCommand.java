package ru.renessans.jvschool.volkov.tm.command.identified.task;

import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;
import ru.renessans.jvschool.volkov.tm.model.Task;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

import java.util.Collection;

public final class TaskClearCommand extends AbstractCommand {

    private static final String CMD_TASK_CLEAR = "task-clear";

    private static final String DESC_TASK_CLEAR = "очистить все задачи";

    private static final String NOTIFY_TASK_CLEAR = "Производится очистка списка задач...";

    @Override
    public String getCommand() {
        return CMD_TASK_CLEAR;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESC_TASK_CLEAR;
    }

    @Override
    public void execute() {
        final String userId = super.serviceLocator.getAuthService().getUserId();
        ViewUtil.getInstance().print(NOTIFY_TASK_CLEAR);
        final Collection<Task> tasks = super.serviceLocator.getTaskService().removeAll(userId);
        ViewUtil.getInstance().print(tasks);
    }


}