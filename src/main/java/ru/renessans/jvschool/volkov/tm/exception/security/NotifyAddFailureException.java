package ru.renessans.jvschool.volkov.tm.exception.security;

import ru.renessans.jvschool.volkov.tm.exception.AbstractRuntimeException;

public final class NotifyAddFailureException extends AbstractRuntimeException {

    private static final String INCORRECT_COMMAND_REGISTRATION =
            "Ошибка! Сбой добавления уведомлений для команд: %s!\n";

    public NotifyAddFailureException(final String message) {
        super(String.format(INCORRECT_COMMAND_REGISTRATION, message));
    }

}